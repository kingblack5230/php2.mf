<?php

namespace App\Model;
use App\Utility\Config;
abstract class model
{
    public static $table = null;
    public static $primary_key = 'id';
    protected $conn;

    public function __construct()
    {
        $this->conn=\App\Services\DBConnection\DBConnection::getInstance();
        $this->conn->init(Config::load('database'));
    }

    public function all()
    {
        return $this->conn->query("SELECT * FROM " . static::$table);
    }

    public function find($id)
    {
        $result = $this->conn->query("SELECT * FROM " . static::$table . " where " . static::$primary_key . " = '$id'");
        return ((array)$result)[0];
    }

    public function remove($id)
    {
        return (boolean)$this->conn->query("DELETE FROM " . static::$table . " where " . static::$primary_key . " = '$id'");
    }
}

//$model = new model();
//$model->find(2);