<?php

namespace App\Utility;

class Config
{
    public static function load($configFile)
    {
        return include CONFIG_PATH . $configFile . ".php";
    }
}