<?php
namespace App\Utility;
// css, js, img, font handler
class Asset{
    public static function __callStatic($name , $fileNames)
    {
        switch ($name) {
            case 'js':
                foreach ($fileNames as $fileName) {
                    $fileName = str_replace(".","/",$fileName);
                    echo '<script src="' . ASSET_PATH . 'js/' . $fileName . '.js" type="text/javascript"></script>';
                }
                break;
            case 'css':
                foreach ($fileNames as $fileName) {
                    $fileName = str_replace(".","/",$fileName);
                    echo '<link rel="stylesheet" href="' . ASSET_PATH . 'css/' . $fileName . '.css" type="text/css">';
                }
                break;
            case 'image':
                foreach ($fileNames as $fileName) {
                    $fileName = str_replace(".","/",$fileName);
                    $fileName = strrev($fileName);
                    $pos = strpos($fileName , '/');
                    $fileName = substr_replace( $fileName , '.' , $pos , strlen('.'));
                    $fileName = strrev($fileName);
                    echo '<img src="' . ASSET_PATH . 'images/' . $fileName . '" >';
                }
                break;

            default:
                echo "Not a valid asset type!";
                die();
        }
    }
}


// Asset::js("jsPath");
