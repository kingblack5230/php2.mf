<?php
namespace App\Core;

use App\Utility\Input;

class Request
{
    public $uri;
    public $ip;
    public $agent;
    public $method;
    public $referer;
    public $params;

    public function __construct()
    {
        $this->uri = $_SERVER['REQUEST_URI'];
        $this->agent = $_SERVER['HTTP_USER_AGENT'];
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->referer = $_SERVER['HTTP_REFERER'] ?? null;
        if(SANITIZE_ALL){
            $this->sanitizeAll();
        }else{
            $this->params = (object)$_REQUEST;
        }
    }

    public function sanitizeAll(){
        $params = new \stdClass;
        foreach ($_REQUEST as $key=>$value){
            $params->$key = Input::clean($value);
        }
        $this->params = $params;
    }

    public function param($key){
        if($this->paramExists($key))
            return $_REQUEST[$key];
        return null;
    }

    public function paramExists($key)
    {
        if(array_key_exists($key,$_REQUEST))
            return $_REQUEST[$key];
        return false;
    }

    public function __get($name)
    {
        if ($this->paramExists($name)){
            return $this->param($name);
        }
    }

}
/*
$r = new Request();
echo $r->param('age')."<br>";
echo $r->age."<br>";
echo $r->uname;
*/
