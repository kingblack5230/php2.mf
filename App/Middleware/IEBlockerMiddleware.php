<?php

namespace App\Middleware;

use App\Core\Request;

class IEBlockerMiddleware extends BaseMiddleware
{
    public function handle(Request $request)
    {
        if (strpos($request->agent, 'Trident/7.0')!==false) {
            echo "IE Browser Blocked...:)";
            die();

        }
    }
}
